const gulp = require('gulp')
const clean = require('gulp-clean')

function cleanTask() {
    return gulp.src('./dist/*').pipe(clean())
}
function cleanJsTask() {
    return gulp.src('./dist/**/*.js').pipe(clean())
}
function cleanCssTask() {
    return gulp.src('./dist/**/*.css').pipe(clean())
}

exports.cleanTask = cleanTask
exports.cleanJsTask = cleanJsTask
exports.cleanCssTask = cleanCssTask
