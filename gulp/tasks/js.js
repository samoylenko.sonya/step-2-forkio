const gulp = require('gulp')
const concat = require('gulp-concat')
const sourcemaps = require('gulp-sourcemaps')
const jsMin = require('gulp-js-minify')
const uglify = require('gulp-uglify')
const babel = require('gulp-babel')

function jsTask() {
    return (
        gulp
            .src('./src/**/*.js', { sourcemaps: true })
            // .pipe(sourcemaps.init())
            .pipe(
                babel({
                    presets: ['@babel/env'],
                })
            )
            .pipe(concat('scripts.min.js'))
            .pipe(uglify())
            // .pipe(sourcemaps.write())
            .pipe(gulp.dest('./dist'))
    )
}

exports.jsTask = jsTask
