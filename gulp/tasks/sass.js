const gulp = require('gulp')
const sass = require('gulp-sass')(require('sass'))
const autoprefixer = require('gulp-autoprefixer')
const concat = require('gulp-concat')
const cleanCss = require('gulp-clean-css')
const sourcemaps = require('gulp-sourcemaps')

function sassTask() {
    return gulp
        .src('./src/**/*.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(sourcemaps.init())
        .pipe(concat('styles.min.css'))
        .pipe(cleanCss())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist'))
}

exports.sassTask = sassTask;