const gulp = require('gulp')
const sassTask = require('./gulp/tasks/sass.js').sassTask
const jsTask = require('./gulp/tasks/js.js').jsTask
const imageTask = require('./gulp/tasks/image.js').imageTask
const {
    cleanTask,
    cleanCssTask,
    cleanJsTask,
} = require('./gulp/tasks/clean.js')
const webserver = require('gulp-webserver')
const browserSync = require('browser-sync').create()

gulp.task('clean', cleanTask)
gulp.task('cleanJs', cleanJsTask)
gulp.task('cleanCss', cleanCssTask)
gulp.task('build:sass', sassTask)
gulp.task('build:js', jsTask)
gulp.task('build:image', imageTask)

gulp.task('watch', function () {
    browserSync.init({
        server: {
            baseDir: './dist',
        },
    })

    gulp.watch('./src/**/*.scss', gulp.series('cleanCss', 'build:sass')).on(
        'change',
        function () {
            browserSync.reload()
        }
    )
    gulp.watch('./src/**/*.js', gulp.series('cleanJs', 'build:js')).on(
        'change',
        function () {
            browserSync.reload()
        }
    )
})

gulp.task('copy-html', function () {
    return gulp.src('./index.html').pipe(gulp.dest('./dist'))
})

gulp.task('webserver', function () {
    gulp.src('dist').pipe(
        webserver({
            livereload: true,
            open: true,
            port: 8000,
        })
    )
})

gulp.task(
    'build',
    gulp.series('clean', 'build:sass', 'build:js', 'build:image', 'copy-html'),
    () => gulp.src('./src/*')
)

gulp.task(
    'default',
    gulp.series('watch'),
    () => gulp.src('./src/*')
)